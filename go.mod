module gitlab.com/masvio/opensource/pubnub-go.git

go 1.16

require (
	github.com/brianolson/cbor_go v1.0.0
	github.com/cucumber/godog v0.12.0
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.5.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.8.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
