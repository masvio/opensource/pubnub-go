package main

import (
	"fmt"

	pubnub "gitlab.com/masvio/opensource/pubnub-go.git"
)

func main() {
	config := pubnub.NewConfigWithUserId(UserId(pubnub.GenerateUUID()))
	config.SubscribeKey = "demo"
	config.PublishKey = "demo"

	pn := pubnub.NewPubNub(config)

	res, status, err := pn.Time().Execute()

	if err != nil {
		fmt.Println("Error: ", err)
	}

	fmt.Println(res, status)
}
