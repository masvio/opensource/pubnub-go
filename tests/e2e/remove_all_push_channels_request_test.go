package e2e

import (
	"testing"

	"github.com/stretchr/testify/assert"
	pubnub "gitlab.com/masvio/opensource/pubnub-go.git"
)

func TestRemoveAllPushNotifications(t *testing.T) {
	assert := assert.New(t)

	pn := pubnub.NewPubNub(configCopy())

	_, _, err := pn.RemoveAllPushNotifications().
		DeviceIDForPush(randomized("di")).
		PushType(pubnub.PNPushTypeGCM).
		Execute()
	assert.Nil(err)
}

func TestRemoveAllPushNotificationsContext(t *testing.T) {
	assert := assert.New(t)

	pn := pubnub.NewPubNub(configCopy())

	_, _, err := pn.RemoveAllPushNotificationsWithContext(backgroundContext).
		DeviceIDForPush(randomized("di")).
		PushType(pubnub.PNPushTypeGCM).
		Execute()
	assert.Nil(err)
}
