package main

import (
	"fmt"
	"os"
	"runtime/pprof"
	"sync/atomic"
	"time"

	pubnub "gitlab.com/masvio/opensource/pubnub-go.git"
)

func main() {
	config := pubnub.NewConfigWithUserId(pubnub.UserId(pubnub.GenerateUUID()))
	config.PNReconnectionPolicy = pubnub.PNExponentialPolicy
	config.MaximumReconnectionRetries = -1
	config.HeartbeatInterval = 60

	ln1 := pubnub.NewListener()

	newCount := atomic.Int64{}
	go func() {
		t := time.NewTicker(5 * time.Second)
		defer t.Stop()

		for {
			<-t.C
			value := newCount.Load()
			fmt.Printf("R: %v\n", value)
		}
	}()

	for {
		pn := pubnub.NewPubNub(config)
		pn.GetClient()

		pn.AddListener(ln1)
		pn.Subscribe().
			Channels([]string{"blah"}).
			Execute()

		done := make(chan bool)
		go func() {
			t := time.NewTicker(time.Second)
			defer t.Stop()

			select {
			case <-done:
				return

			case <-t.C:
				fmt.Printf("DESTROY STUCK!\n")
				pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
				time.Sleep(10 * time.Second)
			}
		}()

		pn.Destroy()
		value := newCount.Load()
		newCount.Store(value + 1)
		close(done)
		pn = nil
	}
}
