package contract

import pubnub "gitlab.com/masvio/opensource/pubnub-go.git"

type commonStateKey struct{}

type commonState struct {
	contractTestConfig contractTestConfig
	pubNub             *pubnub.PubNub
	err                error
	statusResponse     pubnub.StatusResponse
}

func newCommonState(contractTestConfig contractTestConfig) *commonState {

	return &commonState{
		contractTestConfig: contractTestConfig,
	}
}
